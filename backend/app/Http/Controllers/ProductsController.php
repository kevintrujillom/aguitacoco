<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->get();;
        
        if ($products) {

            return response()->json([ 
                'success' => true,
                'status' => 200,
                'response' => $products
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'status' => 404,
            'response' => 'No results'
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('title') && $request->input('price') &&
            $request->input('description') && $request->input('units')) {

            $product = Product::create([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
                'units' => $request->input('units'),
                'discount' => $request->input('discount') ? $request->input('discount') : null ,
                'discount_inite_date' => $request->input('discount_inite_date') ? $request->input('discount_inite_date') : null,
                'discount_end_date' => $request->input('discount_end_date') ? $request->input('discount_end_date') : null,
            ]);
            
            if ($product) {
    
                return response()->json([ 
                    'success' => true,
                    'status' => 200,
                    'response' => "Product {$product->title}, saved successfully."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'success' => false,
                    'status' => 400,
                    'response' => "Product {$product->title}, not saved."
                ], 400);              
            }

        } else {

            return response()->json([ 
                'success' => false,
                'status' => 400,
                'response' => "Some field empty."
            ], 400); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if ($product) {

            return response()->json([ 
                'success' => true,
                'status' => 200,
                'response' => $product
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'status' => 404,
            'response' => 'No results'
        ], 404);        
        
    }

    public function lastProducts()
    {
        $products = Product::latest()
                    ->take(3)
                    ->get();
        
        if ($products) {

            return response()->json([ 
                'success' => true,
                'status' => 200,
                'response' => $products
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'status' => 404,
            'response' => 'No results'
        ], 404);        
    }

    public function productByName($name)
    {
        $products = Product::where('title', 'like', '%'.$name.'%')->get();
        
        if ($products) {

            return response()->json([ 
                'success' => true,
                'status' => 200,
                'response' => $products
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'status' => 404,
            'response' => 'No results'
        ], 404);        
    }    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if ($product) {

            if ($request->input('title') && $request->input('price') &&
                $request->input('description') && $request->input('units')) {
    
                $product->update([
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'price' => $request->input('price'),
                    'units' => $request->input('units'),
                    'discount' => $request->input('discount') ? $request->input('discount') : null ,
                    'discount_inite_date' => $request->input('discount_inite_date') ? $request->input('discount_inite_date') : null,
                    'discount_end_date' => $request->input('discount_end_date') ? $request->input('discount_end_date') : null,
                ]);
                
                if ($product) {
        
                    return response()->json([ 
                        'success' => true,
                        'status' => 200,
                        'response' => "Product {$product->title}, updated successfully."
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'success' => false,
                        'status' => 400,
                        'response' => "Product {$product->title}, not updated."
                    ], 400);              
                }
    
            } else {
    
                return response()->json([ 
                    'success' => false,
                    'status' => 400,
                    'response' => "Some field empty."
                ], 400); 
            }

        }

        return response()->json([ 
            'status' => 400,
            'response' => "Product with id {$id}, not found."
        ], 400);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product) {

            $product->delete();

            return response()->json([ 
                'success' => true,
                'status' => 200,
                'response' => "Product {$product->title}, removed successfully."
            ], 200);

        }

        return response()->json([ 
            'success' => false,
            'status' => 404,
            'response' => "Product with id {$id}, not found."
        ], 404);         
    }
}
