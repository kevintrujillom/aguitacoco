<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/products', [ProductsController::class, 'index']);
Route::get('/products/last-products', [ProductsController::class, 'lastProducts']);
Route::get('/products/products-by-name/{name}', [ProductsController::class, 'productByName']);

Route::get('/products/{id}', [ProductsController::class, 'show']);
Route::get('/products/delete/{id}', [ProductsController::class, 'destroy']);
Route::post('products/store', [ProductsController::class, 'store']);
Route::post('products/update/{id}', [ProductsController::class, 'update']);
