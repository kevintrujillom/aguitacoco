<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $units = rand(1, 15);
        $price = rand(15000, 150000);
        $discount = rand(0, 100); 

        return [
            'title'               => $this->faker->name,
            'description'         => $this->faker->text(20),
            'units'               => $units,
            'price'               => $price,
            'discount'            => $discount,
            'discount_inite_date' => $this->faker->dateTimeBetween('+0 days', '+1 days'),
            'discount_end_date'   => $this->faker->dateTimeBetween('+3 days', '+2 week'),
        ];
    }
}
