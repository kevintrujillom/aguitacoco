# Aguita de Coco

# API (Backend)
```
Laravel: ^8.12
php: ^7.3 | ^8.0,
```

## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado backend/ donde se encuentra el API desarrollado con el framework Laravel de PHP. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
composer install
```

## Base de datos
Como motor de base de datos se implemento:
```
MySQL junto con phpmyadmin
```
Para configurar la base de datos del proyecto se debe tomar como guìa el archivo:
```
.env.example
```
Para cargar algunos datos de prueba ejecutar:
```
php artisan migrate:refresh --seed
```

## Iniciar servidor
Para iniciar el servidor ejecutar:
```
php artisan serve
```
El API debe ser consumida a la siguinete url::
```
http://127.0.0.1:8000/api
```

# Aplicación (Frotend)
```
Angular CLI: 10.2.1
Node: 15.0.1
```
## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado aguitacoco/ donde se encuentra el frontend desarrollado con angular. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
npm install
```
## Iniciar servidor
Para iniciar el servidor ejecutar:
```
ng serve
```
En el navegador de preferencia ingresar el enlace para ver el proyecto:
```
http://localhost:4200/
```

## Nota:
Es importante tener en cuenta que la url para consumir el API es la siguiente (en esa url corre el API desarrollado con Laravel):
```
http://127.0.0.1:8000/api
```
Como se puede observar en el archivo environment.ts que se encuentra en la siguiente ubicación:
```
aguitacoco/src/environments/environment.ts

```

# Tecnologías:
## Implementadas en el API:
```
1. Laravel (PHP)
2. MySQL (Motor de base de datos)
```
## Implementadas en la aplicación:
```
1. Angular (JS)
2. Angular Material (estilos)
```

## Implementadas en el control de versiones:
```
1. Git
2. Bitbucket (repositorio)
```

# Instrucciones de la Aplicacion:
Dentro de ella el usuario puede:
```
1. Ver el home donde estan visibles los productos
2. Ver el detalle de cada uno de los productos al darle cick sobre su respectiva imagen
3. Agregar los productos al carrito de compras
4. Acceder al carrito de compras donde aparecen los productos agregados anteriormente, en esta vista puede eliminar del carrito los productos deseados
5. Simular una compra en el checkout de la tienda, donde se debe llenar un formulario, en esta vista se observa un pequeño resumen de los prouctos a comprar
6. Visualizar una tabla donde podrá realizar el CRUD completo (crear, ver, editar/actualizar y borrar) a los productos
```