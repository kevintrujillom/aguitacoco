import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Product } from './../../models/product/product.model';
import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  products: Product[] = Array();
  product: any = [];

  itemsCart = Array();
  numItemsCart = 0;

  message = {
    error: false,
    content: ''
  };

  constructor( private route: ActivatedRoute, private productsService: ProductsService ) { }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.getProduct(id);
    });

    this.getLastProducts();
    this.numsItemsCart();

  }

  getProduct(id: number){
    this.productsService.getProduct(id).subscribe(product => {

      if (product.success) {

        this.product = product.response;
        this.product.error = false;
        this.message.content = '';

      } else {

        this.message.error = true;
        this.message = product.response;
      }
      },
      error => {
        this.message.error = true;
        this.message.content = error.message;
        console.log('oops ', error);
      }
    );    
  }

  numsItemsCart(){
    let numItems = JSON.parse(localStorage.getItem('itemsCart'));

    if(numItems){
      if(numItems.length > 0){
        this.numItemsCart = numItems.length;
      }
    }
  }

  addToCart(product){

    let products = JSON.parse(localStorage.getItem('itemsCart')) || [];
    let already_exists = false;

    if (products.length > 0) {
      this.itemsCart = products;
    }

    for (let i = 0; i < products.length; i++) {
                        
      if (products[i].id == product.id) {
          already_exists = true;
          alert('Product is already in the cart');
      }
    }                    

    if (!already_exists) {
      this.itemsCart.push(product);
      alert('Product added in the cart!');
    } 
    
    this.numItemsCart = this.itemsCart.length;
    localStorage.setItem('itemsCart', JSON.stringify(this.itemsCart));
  }  

  getLastProducts(){
    this.productsService.getLastProducts().subscribe(products => {
      
      if (products.success) {

        this.products = products.response;
        this.message.error = false;
        this.message.content = '';

      } else {

        this.message.error = true;
        this.message = products.response;
      }

    });
  }  

}
