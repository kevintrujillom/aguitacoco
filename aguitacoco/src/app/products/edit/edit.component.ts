import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  product: any = [];

  message = {
    error: false,
    content: ''
  };

  formEdit: FormGroup;

  constructor( private route: ActivatedRoute, private productsService: ProductsService ) {
    this.formEdit = this.createFormGroup();
  }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.getProduct(id);
    });

  }

  createFormGroup(){

    return new FormGroup({

      title: new FormControl('', [
        Validators.required
      ]),

      description: new FormControl('', [
        Validators.required
      ]),

      units: new FormControl('', [
        Validators.required
      ]),

      price: new FormControl('', [
        Validators.required
      ]),

      discount: new FormControl(),
      discount_inite_date: new FormControl(),
      discount_end_date: new FormControl(),

    });

  }

  getProduct(id: number){

    this.productsService.getProduct(id).subscribe(product => {

      this.product = product.response;

      this.formEdit.patchValue({
        title: this.product.title,
        description: this.product.description,
        units: this.product.units,
        price: this.product.price,
        discount: this.product.discount,
        discount_inite_date: this.product.discount_inite_date,
        discount_end_date: this.product.discount_end_date,
      });

    });

  }

  save(id: number){

    this.message.content = '';

    if (this.formEdit.valid) {

      this.productsService.updateProduct(id, this.formEdit.value).subscribe(
        data => {
          this.message.error = false;
          this.message.content = data.response;
        },
        error => {
          this.message.error = true;
          this.message.content = error.message;
          console.log('oops ', error);
        }
      );

    } else {

      alert('Form cannot be submitted');
    }

  }

  get title(): any { return this.formEdit.get('title'); }
  get description(): any { return this.formEdit.get('description'); }
  get units(): any { return this.formEdit.get('units'); }
  get price(): any { return this.formEdit.get('price'); }
  get discount(): any { return this.formEdit.get('discount'); }
  get discount_inite_date(): any { return this.formEdit.get('discount_inite_date'); }
  get discount_end_date(): any { return this.formEdit.get('discount_end_date'); }

}
