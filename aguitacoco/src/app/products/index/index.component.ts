import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Product } from './../../models/product/product.model';

import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  products: Product[] = Array();

  actualPage: number = 1;

  message = {
    error: false,
    content: ''
  };

  nameProduct = '';
  filter = false;

  formSearch: FormGroup;

  constructor( private productsService: ProductsService ) {
    this.formSearch = this.searchFormGroup();
  }

  ngOnInit(): void {
    this.getAll();
  }

  searchFormGroup(){

    return new FormGroup({

      product: new FormControl(''),

    });

  }

  getAll(){
    this.productsService.getAll().subscribe(products => {

      if (products.success) {

        this.products = products.response;
        this.message.error = false;
        this.message.content = '';

      } else {

        this.message.error = true;
        this.message = products.response;
      }

      this.filter = false;
      this.clearForm();
    });
  }

  delete(id: number){

    this.productsService.deleteProduct(id).subscribe(
      data => {
        this.message.content = data.response;

        setTimeout(() => {
          this.getAll();
        }, 1000);
      },
      error => {
        this.message.error = true;
        this.message.content = error.message;
        console.log('oops ', error);
      }
    );

  }

  searchProducts(){
 
    this.nameProduct = this.formSearch.value.product;

    this.productsService.getProductsByName(this.nameProduct).subscribe(products => {

        if ( products.length === 0 ) {

          this.products = [];
          this.message.error = true;
          this.message.content = 'Not results';

        } else {

          this.products = products.response;
          this.message.error = false;
          this.message.content = '';
        }

        this.filter = true;
      },
      error => {
        this.message.error = true;
        this.message.content = error.message;
        console.log('oops ', error);
      }
    );
  }

  clearForm(){
    this.formSearch.setValue({ product: '' });
  }

}
