import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Product } from './../../models/product/product.model';

import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product[] = Array();
  
  actualPage: number = 1;

  itemsCart = Array();
  numItemsCart = 0;

  message = {
    error: false,
    content: ''
  };

  nameProduct = '';
  filter = false;

  formSearch: FormGroup;

  constructor( private productsService: ProductsService ) { 
    this.formSearch = this.searchFormGroup();
  }

  ngOnInit(): void {
    this.getAll();
    this.numsItemsCart();
  }

  searchFormGroup(){

    return new FormGroup({

      product: new FormControl(''),

    });

  }

  getAll(){
    this.productsService.getAll().subscribe(products => {
      
      if (products.success) {

        this.products = products.response;
        this.message.error = false;
        this.message.content = '';

      } else {

        this.message.error = true;
        this.message = products.response;
      }

      this.filter = false;
      this.clearForm();
    });
  }

  numsItemsCart(){
    let numItems = JSON.parse(localStorage.getItem('itemsCart'));

    if(numItems){
      if(numItems.length > 0){
        this.numItemsCart = numItems.length;
      }
    }
  }

  addToCart(product){

    let products = JSON.parse(localStorage.getItem('itemsCart')) || [];
    let already_exists = false;

    if (products.length > 0) {
      this.itemsCart = products;
    }

    for (let i = 0; i < products.length; i++) {
                        
      if (products[i].id == product.id) {
          already_exists = true;
          alert('Product is already in the cart');
      }
    }                    

    if (!already_exists) {
      this.itemsCart.push(product);
      alert('Product added in the cart!');
    } 
    
    this.numItemsCart = this.itemsCart.length;
    localStorage.setItem('itemsCart', JSON.stringify(this.itemsCart));
  }

  searchProducts(){

    this.nameProduct = this.formSearch.value.product;

    this.productsService.getProductsByName(this.nameProduct).subscribe(products => {

        if ( products.length === 0 ) {

          this.products = [];
          this.message.error = true;
          this.message.content = 'Not results';

        } else {

          this.products = products.response;
          this.message.error = false;
          this.message.content = '';
        }

        this.filter = true;
      },
      error => {
        this.message.error = true;
        this.message.content = error.message;
        console.log('oops ', error);
      }
    );
  }

  clearForm(){
    this.formSearch.setValue({ product: '' });
  }

}
