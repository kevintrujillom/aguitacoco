import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  message = {
    error: false,
    content: ''
  };

  formCreate: FormGroup;

  constructor( private productsService: ProductsService ) {
    this.formCreate = this.createFormGroup();
  }

  ngOnInit(): void {
  }

  createFormGroup(){

    return new FormGroup({

      title: new FormControl('', [
        Validators.required
      ]),

      description: new FormControl('', [
        Validators.required
      ]),

      units: new FormControl('', [
        Validators.required
      ]),

      price: new FormControl('', [
        Validators.required
      ]),

      discount: new FormControl(),
      discount_inite_date: new FormControl(),
      discount_end_date: new FormControl(),

    });

  }

  clearForm(){
    this.formCreate.reset();
  }

  create(){

    this.message.content = '';

    if (this.formCreate.valid) {

      this.productsService.createProduct(this.formCreate.value).subscribe(
        data => {
          this.message.error = false;
          this.message.content = data.response;
          this.clearForm();
        },
        error => {
          this.message.error = true;
          this.message.content = error.message;
          console.log('oops ', error);
        }
      );

    } else {

      alert('Form cannot be submitted, some fields empty');
    }

  }

  get title(): any { return this.formCreate.get('title'); }
  get description(): any { return this.formCreate.get('description'); }
  get units(): any { return this.formCreate.get('units'); }
  get price(): any { return this.formCreate.get('price'); }
  get discount(): any { return this.formCreate.get('discount'); }
  get discount_inite_date(): any { return this.formCreate.get('discount_inite_date'); }
  get discount_end_date(): any { return this.formCreate.get('discount_end_date'); }

}
