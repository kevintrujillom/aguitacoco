import { Component, OnInit } from '@angular/core';

import { Product } from './../../models/product/product.model';

import { ProductsService } from './../../core/services/products/products.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  products: Product[] = Array();

  emptyCart = true;
  total = 0;

  message = {
    error: false,
    content: ''
  };

  constructor( private productsService: ProductsService ) { } 

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    let listProducts = JSON.parse(localStorage.getItem('itemsCart')) || [];

    if (listProducts.length > 0) {

      this.products = listProducts;
      this.message.error = false;
      this.message.content = '';
      this.emptyCart = false;

      this.getTotal();
    
    } else {
      this.products = null;
      this.emptyCart = true;
    }

  }  

  getTotal(){
    this.total = 0;
    let intTotal = 0;
    let subtotal = 0;

    let listProducts = JSON.parse(localStorage.getItem('itemsCart')) || [];

    if (listProducts.length > 0) {

      this.products = listProducts;

      for (let i = 0; i < this.products.length; i++) {
        subtotal = this.products[i].price - (this.products[i].price * (this.products[i].discount/100))
        intTotal = intTotal + subtotal;
      } 

      this.total = intTotal;

      localStorage.setItem('totalCart', JSON.stringify(this.total));
      
    }

  }

  delete(id: number){

    let itemsCart = JSON.parse(localStorage.getItem('itemsCart'));

    itemsCart.forEach(function (currentValue, index, arr) {

        if (itemsCart[index]['id'] == id) {

            if (itemsCart.length == 1){

                localStorage.removeItem('itemsCart');

            } else if (index == 0) {

                itemsCart.shift();
                localStorage.setItem('itemsCart', JSON.stringify(itemsCart));

            } else {

                itemsCart.splice(index, index);
                localStorage.setItem('itemsCart', JSON.stringify(itemsCart));
            }
        }
    });

    this.products = JSON.parse(localStorage.getItem('itemsCart'));

    this.getAll();       
  }

  empty(){
    localStorage.removeItem('itemsCart');
    this.getAll();
  }
}
