export class Product {
    id: number;
    title: string;
    description: string;
    units: number;
    price: number;
    discount: number;
    discount_inite_date: string;
    discount_end_date: string;
}