import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Product } from './../models/product/product.model';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  products: Product[] = Array();

  emptyCart = true;
  total = 0;

  formCreate: FormGroup;

  constructor() { 
    this.formCreate = this.createFormGroup();
  }

  ngOnInit(): void {
    this.getAll();
  }

  createFormGroup(){

    return new FormGroup({

      name: new FormControl('', [
        Validators.required
      ]),

      identification: new FormControl('', [
        Validators.required
      ]),

      email: new FormControl('', [
        Validators.required
      ]),

      phone: new FormControl('', [
        Validators.required
      ]),

      address: new FormControl('', [
        Validators.required
      ]),

      extra: new FormControl(),

    });

  } 

  getAll(){
    let listProducts = JSON.parse(localStorage.getItem('itemsCart')) || [];
    
    if (listProducts.length > 0) {

      this.products = listProducts;

      this.getTotal();
    
    } else {

      this.products = null;
      this.emptyCart = true;
    }

  }  

  getTotal(){
    this.total = 0;
    let intTotal = 0;
    let subtotal = 0;

    let listProducts = JSON.parse(localStorage.getItem('itemsCart')) || [];

    if (listProducts.length > 0) {

      this.products = listProducts;

      for (let i = 0; i < this.products.length; i++) {
        subtotal = this.products[i].price - (this.products[i].price * (this.products[i].discount/100))
        intTotal = intTotal + subtotal;
      } 

      this.total = intTotal;

      localStorage.setItem('totalCart', JSON.stringify(this.total));
      
    }

  }  

  purchase(){

    if (this.formCreate.valid) {

      alert('Successful purchase!');

      localStorage.removeItem('itemsCart');

      setTimeout(() => {
        location.href ="/";
      }, 1000);

    } else {

      alert('Form cannot be submitted, some fields empty...');
    }
  }

  get name(): any { return this.formCreate.get('name'); }
  get identification(): any { return this.formCreate.get('identification'); }
  get email(): any { return this.formCreate.get('email'); }
  get phone(): any { return this.formCreate.get('phone'); }
  get address(): any { return this.formCreate.get('address'); }
  get extra(): any { return this.formCreate.get('extra'); } 

}
