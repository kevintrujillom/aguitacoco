import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
}) 
export class ProductsService {

  constructor( private http: HttpClient ) { }

  getAll(): any {
    return this.http.get(`${environment.url_api}/products/`)
  }

  getProduct(id: number): any {
    return this.http.get(`${environment.url_api}/products/${id}`)
  }

  getLastProducts(): any {
    return this.http.get(`${environment.url_api}/products/last-products`)
  }  

  createProduct(product): any {
    return this.http.post(`${environment.url_api}/products/store`, product)
  }

  updateProduct(id: number, changes): any {
    return this.http.post(`${environment.url_api}/products/update/${id}`, changes)
  }  

  deleteProduct(id: number): any {
    return this.http.get(`${environment.url_api}/products/delete/${id}`)
  }

  // Get products by name
  getProductsByName(name: string): any {
    return this.http.get(`${environment.url_api}/products/products-by-name/${name}`);
  }
}
